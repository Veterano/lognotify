import Vue from 'nativescript-vue'
import App from './components/App'
import store from './store'

  

Vue.config.silent = (TNS_ENV === 'production')
Vue.registerElement('CardView', () => require('@nstudio/nativescript-cardview').CardView);
Vue.registerElement("Gradient", () => require("nativescript-gradient").Gradient);

new Vue({
  store,
  render: h => h(App),
}).$start()
